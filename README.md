# eupdater-spring-boot-starter


#### 安装教程
[![](https://www.jitpack.io/v/com.gitee.lp-yly/eupdater-spring-boot-starter.svg)](https://www.jitpack.io/#com.gitee.lp-yly/eupdater-spring-boot-starter)

**- maven**
```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://www.jitpack.io</url>
    </repository>
</repositories>

<dependency>
    <groupId>com.gitee.lp-yly</groupId>
    <artifactId>eupdater-spring-boot-starter</artifactId>
    <version>LATEST</version>
</dependency>
```
**- gradle**
```gradle
allprojects {
    repositories {
        ...
        maven { url 'https://www.jitpack.io' }
    }
}

dependencies {
    implementation 'com.gitee.lp-yly:eupdater-spring-boot-starter:LATEST'
}

```
1. 判断设备激活

```
@Resource
private TerminalCtr terminalCtr;

boolean actived=terminalCtr.isDeviceLicense(deviceId)
```





