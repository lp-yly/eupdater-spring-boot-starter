package elijah.eupdater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EupdaterSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(EupdaterSpringBootStarterApplication.class, args);
    }

}
