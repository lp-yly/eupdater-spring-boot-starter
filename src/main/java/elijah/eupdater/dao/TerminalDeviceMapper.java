package elijah.eupdater.dao;

import elijah.eupdater.entity.TerminalDevice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TerminalDeviceMapper {
    int deleteByPrimaryKey(@Param("deviceId") String deviceId, @Param("packageName") String packageName);

    int insert(TerminalDevice record);

    int insertSelective(TerminalDevice record);

    TerminalDevice selectByPrimaryKey(@Param("deviceId") String deviceId, @Param("packageName") String packageName);

    int updateByPrimaryKeySelective(TerminalDevice record);

    int updateByPrimaryKey(TerminalDevice record);

    List<TerminalDevice> selectAll(@Param("terminal") TerminalDevice terminal, @Param("current") Integer current, @Param("pageSize") Integer pageSize);


    int updateTerminalDeviceApp(@Param("terminalDevice") TerminalDevice terminalDevice);

    List<TerminalDevice> getUnregisteredDevices();


    int registeredDevice(@Param("terminalDevice") TerminalDevice terminalDevice);

    int isDeviceLicense(@Param("deviceId") String deviceId);

    int checkCanUpdate(@Param("deviceId") String deviceId, @Param("packageName") String packageName, @Param("maxUpdateCount") int maxUpdateCount);

    int startUpdate(@Param("deviceId") String deviceId, @Param("packageName") String packageName);

    int endUpdate(@Param("deviceId") String deviceId, @Param("packageName") String packageName);
}