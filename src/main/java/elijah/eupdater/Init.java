package elijah.eupdater;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("eupdater-init")
public class Init {
    private Logger logger = LoggerFactory.getLogger(Init.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @PostConstruct
    public void initSql() {
        jdbcTemplate.execute("IF OBJECT_ID('terminal_device', 'U') IS NOT NULL\n" +
                "    BEGIN\n" +
                "        IF NOT EXISTS (SELECT 1\n" +
                "                       FROM sys.columns\n" +
                "                       WHERE Name = 'updating'\n" +
                "                         AND Object_ID = Object_ID('terminal_device'))\n" +
                "            DROP TABLE terminal_device\n" +
                "    END\n" +
                "IF NOT EXISTS (SELECT *\n" +
                "               FROM sys.tables\n" +
                "               WHERE name = 'terminal_device')\n" +
                "    BEGIN\n" +
                "        CREATE TABLE terminal_device\n" +
                "        (\n" +
                "            device_id           varchar(50) not null,\n" +
                "            package_name        varchar(50) not null,\n" +
                "            device_tag          varchar(100),\n" +
                "            device_ip           varchar(100),\n" +
                "            license             varchar(50),\n" +
                "            version_code        int,\n" +
                "            version_name        varchar(100),\n" +
                "            update_version_code int,\n" +
                "            update_version_name varchar(100),\n" +
                "            update_file_name    varchar(100),\n" +
                "            update_file_path    varchar(100),\n" +
                "            update_type         char(1),--F:FTP, H:HTTP\n" +
                "            updating              bit default 0,\n" +
                "            manual              bit default 1,\n" +
                "            comment             varchar(100),\n" +
                "            create_time         datetime,\n" +
                "            update_time         datetime,\n" +
                "            primary key (device_id, package_name)\n" +
                "        )\n" +
                "    END\n" +
                "IF NOT EXISTS (SELECT *\n" +
                "               FROM sys.tables\n" +
                "               WHERE name = 'terminal_auth')\n" +
                "    BEGIN\n" +
                "        CREATE TABLE terminal_auth\n" +
                "        (\n" +
                "            password varchar(50) not null,\n" +
                "        )\n" +
                "        insert into terminal_auth (password) values ('admin')\n" +
                "    END\n");

        logger.info("终端更新服务已开启");

    }

}
