package elijah.eupdater.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TerminalDevice {

    private Integer sort;
    private String deviceId;
    private String license;
    private Boolean activated;
    private String packageName;

    private String deviceTag;

    private String deviceIp;


    private Long versionCode;

    private String versionName;

    private Long updateVersionCode;

    private String updateVersionName;

    private String updateFilePath;
    private String updateFileName;

    private String updateType;
    private Boolean manual;

    private String comment;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTimeStart;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTimeEnd;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTimeStart;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTimeEnd;

    public Date getCreateTimeStart() {
        return createTimeStart;
    }

    public TerminalDevice setCreateTimeStart(Date createTimeStart) {
        this.createTimeStart = createTimeStart;
        return this;
    }

    public Date getCreateTimeEnd() {
        return createTimeEnd;
    }

    public TerminalDevice setCreateTimeEnd(Date createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
        return this;
    }

    public String getUpdateFileName() {
        return updateFileName;
    }

    public TerminalDevice setUpdateFileName(String updateFileName) {
        this.updateFileName = updateFileName;
        return this;
    }

    public Boolean getManual() {
        return manual;
    }

    public TerminalDevice setManual(Boolean manual) {
        this.manual = manual;
        return this;
    }

    public Date getUpdateTimeStart() {
        return updateTimeStart;
    }

    public TerminalDevice setUpdateTimeStart(Date updateTimeStart) {
        this.updateTimeStart = updateTimeStart;
        return this;
    }

    public Date getUpdateTimeEnd() {
        return updateTimeEnd;
    }

    public TerminalDevice setUpdateTimeEnd(Date updateTimeEnd) {
        this.updateTimeEnd = updateTimeEnd;
        return this;
    }

    public Boolean isActivated() {
        return activated;
    }

    public TerminalDevice setActivated(Boolean activated) {
        this.activated = activated;
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDeviceTag() {
        return deviceTag;
    }

    public void setDeviceTag(String deviceTag) {
        this.deviceTag = deviceTag;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Long getVersionCode() {
        return versionCode;
    }


    public Boolean getActivated() {
        return activated;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public Integer getSort() {
        return sort;
    }

    public TerminalDevice setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Long getUpdateVersionCode() {
        return updateVersionCode;
    }

    public void setUpdateVersionCode(Long updateVersionCode) {
        this.updateVersionCode = updateVersionCode;
    }

    public String getUpdateVersionName() {
        return updateVersionName;
    }

    public void setUpdateVersionName(String updateVersionName) {
        this.updateVersionName = updateVersionName;
    }

    public String getUpdateFilePath() {
        return updateFilePath;
    }

    public void setUpdateFilePath(String updateFilePath) {
        this.updateFilePath = updateFilePath;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}