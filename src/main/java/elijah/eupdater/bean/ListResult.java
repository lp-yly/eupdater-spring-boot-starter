package elijah.eupdater.bean;

import java.util.List;

public class ListResult<T> {

    private int code;
    private String msg;
    private List<T> data;
    private int total;
    private long time = System.currentTimeMillis();

    public ListResult() {
    }

    public int getTotal() {
        return total;
    }

    public ListResult<T> setTotal(int total) {
        this.total = total;
        return this;
    }

    public ListResult(List<T> data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public ListResult<T> setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public ListResult<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public List<T> getData() {
        return data;
    }

    public ListResult<T> setData(List<T> data) {
        this.data = data;
        return this;
    }

    public long getTime() {
        return time;
    }

    public ListResult<T> setTime(long time) {
        this.time = time;
        return this;
    }
}
