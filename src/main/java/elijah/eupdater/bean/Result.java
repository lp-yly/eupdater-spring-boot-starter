package elijah.eupdater.bean;


public class Result<T> {

    private int code;
    private String msg;
    private T data;
    private long time = System.currentTimeMillis();

    public Result() {
    }



    public Result(int code) {
        this.code = code;
    }

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(T data) {
        this.data = data;
        if (data == null) {
            code = -1;
            msg = "数据不存在";
        }
    }

    public int getCode() {
        return code;
    }

    public Result<T> setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public Result<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public T getData() {
        return data;
    }

    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }

    public long getTime() {
        return time;
    }

    public Result<T> setTime(long time) {
        this.time = time;
        return this;
    }
}
